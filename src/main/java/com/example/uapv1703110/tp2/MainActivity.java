package com.example.uapv1703110.tp2;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.content.Context;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        BookDbHelper dbhelper = new BookDbHelper(MainActivity.this);

        Cursor cursor =dbhelper.fetchAllBooks();
        if (!cursor.moveToFirst()) {
            dbhelper.populate();
        }
        cursor =dbhelper.fetchAllBooks();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] { "title","authors" },
                new int[] { android.R.id.text1, android.R.id.text2});
        final ListView listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(adapter);
        registerForContextMenu(listview);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                BookDbHelper dbhelper = new BookDbHelper(MainActivity.this);
                Cursor c = (Cursor) parent.getItemAtPosition(position);
                Book b = dbhelper.cursorToBook(c);
                Intent intent =new Intent(MainActivity.this,BookActivity.class);
                intent.putExtra("book",b);
                startActivity(intent);
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(MainActivity.this,BookActivity.class);
                intent.putExtra("book",(Parcelable[])null);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        BookDbHelper dbhelper = new BookDbHelper(MainActivity.this);
        Cursor cursor =dbhelper.fetchAllBooks();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] { "title","authors" },
                new int[] { android.R.id.text1, android.R.id.text2});
        final ListView listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(adapter);
        registerForContextMenu(listview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        if (v.getId() == R.id.listView){
            AdapterView.AdapterContextMenuInfo info =(AdapterView.AdapterContextMenuInfo)menuInfo;
            MenuItem mnu1=menu.add(0,0,0,"Supprimer");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView v = findViewById(R.id.listView);
        Cursor c = (Cursor)v.getItemAtPosition(info.position);
        BookDbHelper dbhelper = new BookDbHelper(MainActivity.this);
        dbhelper.deleteBook(c);
        onResume();
        return true;
    }
}
