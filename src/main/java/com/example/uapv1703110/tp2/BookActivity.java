package com.example.uapv1703110.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        Intent intent = getIntent();
        TextView nameView = (TextView) findViewById(R.id.nameBook);
        TextView authorView = (TextView) findViewById(R.id.editAuthors);
        TextView yearView = (TextView) findViewById(R.id.editYear);
        TextView genresView = (TextView) findViewById(R.id.editGenres);
        TextView publisherView = (TextView) findViewById(R.id.editPublisher);
        Book b = (Book) intent.getParcelableExtra("book");
        if (b != null) {
            nameView.setText(b.getTitle());
            authorView.setText(b.getAuthors());
            yearView.setText(b.getYear());
            genresView.setText(b.getGenres());
            publisherView.setText(b.getPublisher());
        }
    }

    public void saveBook(View view) {
        TextView nameView = (TextView) findViewById(R.id.nameBook);
        TextView authorView = (TextView) findViewById(R.id.editAuthors);
        TextView yearView = (TextView) findViewById(R.id.editYear);
        TextView genresView = (TextView) findViewById(R.id.editGenres);
        TextView publisherView = (TextView) findViewById(R.id.editPublisher);
        Intent intent = getIntent();
        Book b = (Book) intent.getParcelableExtra("book");
        if (b != null) {

            b.setTitle(nameView.getText().toString());
            b.setAuthors(authorView.getText().toString());
            b.setYear(yearView.getText().toString());
            b.setGenres(genresView.getText().toString());
            b.setPublisher(publisherView.getText().toString());
            BookDbHelper dbhelper = new BookDbHelper(BookActivity.this);
            dbhelper.updateBook(b);
        }
        else {
            Book livre = new Book();
            livre.setTitle(nameView.getText().toString());
            livre.setAuthors(authorView.getText().toString());
            livre.setYear(yearView.getText().toString());
            livre.setGenres(genresView.getText().toString());
            livre.setPublisher(publisherView.getText().toString());
            BookDbHelper dbhelper = new BookDbHelper(BookActivity.this);
            dbhelper.addBook(livre);
        }
        finish();
    }
}
