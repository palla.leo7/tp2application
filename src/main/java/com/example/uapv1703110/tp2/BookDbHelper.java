package com.example.uapv1703110.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;

import java.util.ArrayList;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";


    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String createBookTable = "CREATE TABLE " + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_BOOK_TITLE + " TEXT, " + COLUMN_AUTHORS + " TEXT, " + COLUMN_YEAR + " TEXT, " + COLUMN_GENRES + " TEXT, " + COLUMN_PUBLISHER + " TEXT)";
        db.execSQL(createBookTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE "+ TABLE_NAME);
        onCreate(db);

    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_NAME);
    }


   /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
	    ContentValues bookInfo= new ContentValues();
	    bookInfo.put(COLUMN_BOOK_TITLE,book.getTitle());
        bookInfo.put(COLUMN_AUTHORS,book.getAuthors());
        bookInfo.put(COLUMN_YEAR,book.getYear());
        bookInfo.put(COLUMN_GENRES,book.getGenres());
        bookInfo.put(COLUMN_PUBLISHER,book.getPublisher());

        db.insert(TABLE_NAME,null,bookInfo);
        db.close(); // Closing database connection
        return (true);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book b) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BOOK_TITLE,b.getTitle()); //These Fields should be your String values of actual column names
        cv.put(COLUMN_AUTHORS,b.getAuthors());
        cv.put(COLUMN_YEAR,b.getYear());
        cv.put(COLUMN_GENRES,b.getGenres());
        cv.put(COLUMN_PUBLISHER,b.getPublisher());
        db.update(TABLE_NAME,cv, "_id = "+Long.toString(b.getId()), null);
        return 1;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();
	    Cursor cursor;
	    // call db.query()
        cursor = db.rawQuery("select * from "+ TABLE_NAME,null);
        cursor.moveToFirst();
        return cursor;
    }

    public Book getBook(int position) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        int pos = position+1;
        cursor = db.rawQuery("select * from "+ TABLE_NAME + " WHERE " + _ID +" = '" + pos + "'",null);
        Book livre = null;
        if (cursor.moveToFirst()) {
            do {
                livre = new Book();
                livre.setId(Integer.parseInt(cursor.getString(0)));
                livre.setTitle(cursor.getString(1));
                livre.setAuthors(cursor.getString(2));
                livre.setYear(cursor.getString(3));
                livre.setGenres(cursor.getString(4));
                livre.setPublisher(cursor.getString(5));
            } while (cursor.moveToNext());
            return livre;
        }
        else {
            return null;
        }
    }

    public void deleteBook(Cursor c) {
        SQLiteDatabase db = this.getWritableDatabase();
        int id = c.getInt(0);
        db.delete(TABLE_NAME, _ID + "=" + Integer.toString(id), null);
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book livre = null;
	// build a Book object from cursor
        if (cursor.getCount() > 0) {
            livre = new Book();
            livre.setId(Integer.parseInt(cursor.getString(0)));
            livre.setTitle(cursor.getString(1));
            livre.setAuthors(cursor.getString(2));
            livre.setYear(cursor.getString(3));
            livre.setGenres(cursor.getString(4));
            livre.setPublisher(cursor.getString(5));
        }
        return livre;
    }
}
